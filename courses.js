fetch("http://localhost:4000/courses")
  .then((res) => res.json())
  .then((data) => {
    // console.log(data);
    // Activity
    let header = document.querySelector("header");
    header.innerHTML = `<h1>${data[0].name}</h1>`;
  });

// document refers to the HTML file connected to our JS file. One of the methods that can be used here is querySelector(), which allows us to "get" a specific HTML element (Only one at a time)
// console.dir(header);
